#!/usr/bin/env bash
#
# Written by: Shai K(mondbev)
# Date: 13/08/2021 (Friday :o)
# Credit goes to: https://github.com/kelseyhightower/kubernetes-the-hard-way
# This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
# http://creativecommons.org/licenses/by-nc-sa/4.0/
# Prerequisites #  Ubuntu, sudo, CURL  

gcpinit(){
gcloud init
gcloud auth login
gcloud config set compute/region us-west1
gcloud config set compute/zone us-west1-c
echo
}
whichBoss(){
    local app=$(which $1)
    [[ -n "$app" ]] && echo 0 || echo 1
}

if whichBoss "gcloud" -eq 1;then
snap install google-cloud-sdk --classic
fi
echo "Will start Gcloud init process"
echo; read -rsn1 -p "Press [ENTER] to continue" key; echo
if [[ $key == '' ]]; then
gcpinit
fi

# The cfssl and cfssljson command line utilities will be used to provision a PKI Infrastructure and generate TLS certificates.

# Download and install cfssl and cfssljson:
wget -q --show-progress --https-only --timestamping \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssl \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssljson
chmod +x cfssl cfssljson
sudo mv cfssl cfssljson /usr/local/bin/

if whichBoss "cfssl" -eq 1;then
logger "$0 can't proceed without cfssl"
fi

# The kubectl command line utility is used to interact with the Kubernetes API Server. 

# Download and install kubectl from the official release binaries:
wget https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/

if whichBoss "kubectl" -eq 1;then
logger "$0 can't proceed without kubectl"
fi

# Virtual Private Cloud Network

gcloud compute networks create mondbev-the-hard-way --subnet-mode custom
gcloud compute networks subnets create kubernetes \
  --network mondbev-the-hard-way \
  --range 10.240.0.0/24

gcloud compute firewall-rules create mondbev-the-hard-way-allow-internal \
  --allow tcp,udp,icmp \
  --network mondbev-the-hard-way \
  --source-ranges 10.240.0.0/24,10.200.0.0/16

gcloud compute firewall-rules create mondbev-the-hard-way-allow-external \
  --allow tcp:22,tcp:6443,icmp \
  --network mondbev-the-hard-way \
  --source-ranges 0.0.0.0/0

gcloud compute firewall-rules list --filter="network:mondbev-the-hard-way"

# Kubernetes Public IP Address

gcloud compute addresses create mondbev-the-hard-way \
  --region $(gcloud config get-value compute/region)

gcloud compute addresses list --filter="name=('mondbev-the-hard-way')"

# Compute Instances

#The compute instances in this lab will be provisioned using Ubuntu Server 20.04, which has good support for the containerd container runtime. 
#Each compute instance will be provisioned with a fixed private IP address to simplify the Kubernetes bootstrapping process.

for i in 0 1 2; do
  gcloud compute instances create controller-${i} \
    --async \
    --boot-disk-size 200GB \
    --can-ip-forward \
    --image-family ubuntu-2004-lts \
    --image-project ubuntu-os-cloud \
    --machine-type e2-standard-2 \
    --private-network-ip 10.240.0.1${i} \
    --scopes compute-rw,storage-ro,service-management,service-control,logging-write,monitoring \
    --subnet kubernetes \
    --tags mondbev-the-hard-way,controller
done

# Kubernetes Workers

#Each worker instance requires a pod subnet allocation from the Kubernetes cluster CIDR range. 
#The pod subnet allocation will be used to configure container networking in a later exercise. 
#The pod-cidr instance metadata will be used to expose pod subnet allocations to compute instances at runtime.

for i in 0 1 2; do
  gcloud compute instances create worker-${i} \
    --async \
    --boot-disk-size 200GB \
    --can-ip-forward \
    --image-family ubuntu-2004-lts \
    --image-project ubuntu-os-cloud \
    --machine-type e2-standard-2 \
    --metadata pod-cidr=10.200.${i}.0/24 \
    --private-network-ip 10.240.0.2${i} \
    --scopes compute-rw,storage-ro,service-management,service-control,logging-write,monitoring \
    --subnet kubernetes \
    --tags mondbev-the-hard-way,worker
done

gcloud compute instances list --filter="tags.items=mondbev-the-hard-way"

# Test SSH
for i in 0 1 2; do
 gcloud compute ssh controller-${i} \
 --command="echo -en \"\n\n\"; exit" #simulate two Enter clicks for ssh-keygen
done

echo "Will start Provisioning a CA and Generating TLS Certificates"
echo; read -rsn1 -p "Press [ENTER] to continue" key; echo
if [[ $key == '' ]]; then
source opotato-certs.sh
fi

# Distribute the Client and Server Certificates
for instance in worker-0 worker-1 worker-2; do
  gcloud compute scp ca.pem ${instance}-key.pem ${instance}.pem ${instance}:~/
done
for instance in controller-0 controller-1 controller-2; do
  gcloud compute scp ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem ${instance}:~/
done

echo "Will start Generating Kubernetes Configuration Files for Authentication"
echo; read -rsn1 -p "Press [ENTER] to continue" key; echo
if [[ $key == '' ]]; then
source opotato-kubernetes-configuration-files.sh
fi

for instance in worker-0 worker-1 worker-2; do
  gcloud compute scp ${instance}.kubeconfig kube-proxy.kubeconfig ${instance}:~/
done
for instance in controller-0 controller-1 controller-2; do
  gcloud compute scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig ${instance}:~/
done

# Generating the Data Encryption Config and Key

#Kubernetes stores a variety of data including cluster state, application configurations, and secrets. Kubernetes supports the ability to encrypt cluster data at rest.
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
#Create the encryption-config.yaml encryption config file:
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
#Copy the encryption-config.yaml encryption config file to each controller instance:
for instance in controller-0 controller-1 controller-2; do
  gcloud compute scp encryption-config.yaml ${instance}:~/
done

# Bootstrapping the etcd Cluster

#Kubernetes components are stateless and store cluster state in etcd. In this lab you will bootstrap a three node etcd cluster and configure it for high availability and secure remote access.
#The commands in this lab must be run on each controller instance: controller-0, controller-1, and controller-2. Login to each controller instance using the gcloud command. Example:

for i in 0 1 2; do
 gcloud compute ssh controller-${i} \
 --command="git clone https://gitlab.com/mondbev/gcloud-config-k8s.git; mv gcloud-config-k8s/* .; bash gcloud-config-controller.sh;rm -rf ./{.git,gcloud-config-k8s} && exit"
done

# Bootstrapping the Kubernetes Control Plane

#In this lab you will bootstrap the Kubernetes control plane across three compute instances and configure it for high availability. 
#You will also create an external load balancer that exposes the Kubernetes API Servers to remote clients. The following components will be installed on each node: Kubernetes API Server, Scheduler, and Controller Manager.

for i in 0 1 2; do
 gcloud compute ssh controller-${i} \
 --command="git clone https://gitlab.com/mondbev/gcloud-config-k8s.git; mv gcloud-config-k8s/* .; bash gcloud-bootstrap-k8s-controller.sh;rm -rf ./{.git,gcloud-config-k8s} && exit"
done

# RBAC for Kubelet Authorization

#In this section you will configure RBAC permissions to allow the Kubernetes API Server to access the Kubelet API on each worker node. Access to the Kubelet API is required for retrieving metrics, logs, and executing commands in pods.
#The commands in this section will effect the entire cluster and only need to be run once from one of the controller nodes.
gcloud compute ssh controller-0 \
--command="git clone https://gitlab.com/mondbev/gcloud-config-k8s.git; mv gcloud-config-k8s/* .; bash rbac-for-kubelet-authorization.sh;rm -rf ./{.git,gcloud-config-k8s} && exit"

{
  KUBERNETES_PUBLIC_ADDRESS=$(gcloud compute addresses describe mondbev-the-hard-way \
    --region $(gcloud config get-value compute/region) \
    --format 'value(address)')

  gcloud compute http-health-checks create kubernetes \
    --description "Kubernetes Health Check" \
    --host "kubernetes.default.svc.cluster.local" \
    --request-path "/healthz"

  gcloud compute firewall-rules create mondbev-the-hard-way-allow-health-check \
    --network mondbev-the-hard-way \
    --source-ranges 209.85.152.0/22,209.85.204.0/22,35.191.0.0/16 \
    --allow tcp

  gcloud compute target-pools create kubernetes-target-pool \
    --http-health-check kubernetes

  gcloud compute target-pools add-instances kubernetes-target-pool \
   --instances controller-0,controller-1,controller-2

  gcloud compute forwarding-rules create kubernetes-forwarding-rule \
    --address ${KUBERNETES_PUBLIC_ADDRESS} \
    --ports 6443 \
    --region $(gcloud config get-value compute/region) \
    --target-pool kubernetes-target-pool
}

#Verification
KUBERNETES_PUBLIC_ADDRESS=$(gcloud compute addresses describe mondbev-the-hard-way \
  --region $(gcloud config get-value compute/region) \
  --format 'value(address)')
curl --cacert ca.pem https://${KUBERNETES_PUBLIC_ADDRESS}:6443/version

# Bootstrapping the Kubernetes Worker Nodes

#In this lab you will bootstrap three Kubernetes worker nodes. 
#The following components will be installed on each node: runc, container networking plugins, containerd, kubelet, and kube-proxy.

for i in 0 1 2; do
 gcloud compute ssh worker-${i} \
 --command="git clone https://gitlab.com/mondbev/gcloud-config-k8s.git; mv gcloud-config-k8s/* .; bash gcloud-bootstrp-k8s-worker.sh;rm -rf ./{.git,gcloud-config-k8s} && exit"
done

#Verification
gcloud compute ssh controller-0 \
  --command "kubectl get nodes --kubeconfig admin.kubeconfig"

# Configuring kubectl for Remote Access
#In this lab you will generate a kubeconfig file for the kubectl command line utility based on the admin user credentials.

{
  KUBERNETES_PUBLIC_ADDRESS=$(gcloud compute addresses describe mondbev-the-hard-way \
    --region $(gcloud config get-value compute/region) \
    --format 'value(address)')

  kubectl config set-cluster mondbev-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443

  kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem

  kubectl config set-context mondbev-the-hard-way \
    --cluster=mondbev-the-hard-way \
    --user=admin

  kubectl config use-context mondbev-the-hard-way
}

#Verification
kubectl get nodes

# Provisioning Pod Network Routes
#Pods scheduled to a node receive an IP address from the node's Pod CIDR range. 
#At this point pods can not communicate with other pods running on different nodes due to missing network routes.

#In this lab you will create a route for each worker node that maps the node's Pod CIDR range to the node's internal IP address.

for instance in worker-0 worker-1 worker-2; do
  gcloud compute instances describe ${instance} \
    --format 'value[separator=" "](networkInterfaces[0].networkIP,metadata.items[0].value)'
done

for i in 0 1 2; do
  gcloud compute routes create kubernetes-route-10-200-${i}-0-24 \
    --network mondbev-the-hard-way \
    --next-hop-address 10.240.0.2${i} \
    --destination-range 10.200.${i}.0/24
done

#Verification
gcloud compute routes list --filter "network: mondbev-the-hard-way"

# Deploying the DNS Cluster Add-on
#In this lab you will deploy the DNS add-on which provides DNS based service discovery, backed by CoreDNS, to applications running inside the Kubernetes cluster.

kubectl apply -f coredns-1.8.yaml

#Verification
kubectl get pods -l k8s-app=kube-dns -n kube-system

kubectl run busybox --image=busybox:1.28 --command -- sleep 3600
kubectl get pods -l run=busybox
POD_NAME=$(kubectl get pods -l run=busybox -o jsonpath="{.items[0].metadata.name}")
kubectl exec -ti $POD_NAME -- nslookup kubernetes

#In this lab you will complete a series of tasks to ensure your Kubernetes cluster is functioning correctly.
# Data Encryption
kubectl create secret generic mondbev-the-hard-way \
  --from-literal="mykey=mydata"
gcloud compute ssh controller-0 \
  --command "sudo ETCDCTL_API=3 etcdctl get \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem\
  /registry/secrets/default/mondbev-the-hard-way | hexdump -C"

# Deployments
#In this section you will verify the ability to create and manage Deployments.
kubectl create deployment nginx --image=nginx

#Verification
kubectl get pods -l app=nginx

# Logs
#In this section you will verify the ability to retrieve container logs.
kubectl logs $POD_NAME

# Exec
#In this section you will verify the ability to execute commands in a container.
kubectl exec -ti $POD_NAME -- nginx -v

# Services
#In this section you will verify the ability to expose applications using a Service.
kubectl expose deployment nginx --port 80 --type NodePort
NODE_PORT=$(kubectl get svc nginx \
  --output=jsonpath='{range .spec.ports[0]}{.nodePort}')
gcloud compute firewall-rules create mondbev-the-hard-way-allow-nginx-service \
  --allow=tcp:${NODE_PORT} \
  --network mondbev-the-hard-way

EXTERNAL_IP=$(gcloud compute instances describe worker-0 \
  --format 'value(networkInterfaces[0].accessConfigs[0].natIP)')
curl -I http://${EXTERNAL_IP}:${NODE_PORT}


